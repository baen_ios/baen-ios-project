BAEN iOS Develpment Plan
=========================

Version 1.x
===========

     Task No| Description                              | Start Time | Status
     ------:| ---------------------------------------- | ----------:| ----------:
     1      |  Optimization of Ad Details View         | Aug 1     | In Progress    

     Iteration 02: Photo Browser

     Build             | Start Time     | Time Taken   | Description                         
     ----------------- | ---------------|------------  |------------
     Build 1.2.70      | Aug 1          | 1 day        | Optimization of AdDetailView 
     Build 1.2.81      | Aug 2          | 1 day        | Add ASIHTTPRequest
     Build 1.2.95      | Aug 6          | 1 day        | TRLHTTPRequest
     Build 1.2         | Aug 7          | 2 day        | Refactor Ads List View and Ads Detail View
     Build 1.2         | Aug 10         | 1 day        | Photo Browser in Ads List
     Build 1.2         | Aug 14         | 1 day        | One-Click Call in Map View

     Iteration 01: Photo Browser

     Build             | Start Time     | Time Taken   | Description                         
     ----------------- | ---------------|------------  |------------
                       | Jul 16         | 1 day        | GWRLoadingView 
                       | Jul 17         | 1 day        | Photo Zoomer 
                       | Jul 18         | 2 day        | Photo Browser 
                       | Jul 27         | 1 day        | Restructure of code 
    Build 1.2.63       | Jul 28         | 1 day        | TestFlight 
                       | Jul 30         | 1 day        | Upgrade 3rd Libs

Version 2.x - Refactor
===========

     Task No| Description                              | Start Time | Status
     ------:| ---------------------------------------- | ----------:| ----------:
     1      |  BAEN 2.0  - Project Starter             | Jul 20     | In Progress    

     Iteration 01: Starter

     Build             | Start Time     | Time Taken   | Description                         
     ----------------- | ---------------|------------  |:------------
      2.0.00001        | Jul 20         | 1 day        | Starter
                       | Jul 21         | 1 day        | Project Architecture
                       | Jul 22         | 1 day        | BeeHive
                       | Jul 23         | 1 day        | User Center
                       | Jul 24         | 1 day        | TRLTool, TRLFailedView
      2.0.10001        | Jul 25         | 1 day        | UIView+Borders
      2.0.10001        | Jul 25         | 1 day        |TRLUserCenterTableViewCell 
 
Development Requirements: BAEN iOS 1.x and 2.x will be developing at the same time
----------------

(1) 1.x - More New Features and Existing Functions Enhancement 
----------------
- * 1.x will focus on adding the necessary features
- * 1.x will perfect existing function

BAEN iOS 1.x Important Features List:

     -----------------------------
    | > App Indexing and Deep Link|
    | > Search Filter             |
    | > ......                    |
     -----------------------------

(2) 2.x - BAEN iOS Framework Reconstruction
----------------
- * 2.x will focus on user experience and UI design
- * 2.x will be adaptable to the various screens sizes
- * 2.x will enhance following aspects:

BAEN iOS 1.x Important Features List:

     --------------------------
    | > Geolocation Feature    |
    | > Advanced Search Feature|
    | > BAEN Message Board     |
    | > ......                 |
     --------------------------

(3) Server-side Functions for BAEN iOS App Client
----------------
- * PHP Interface Optimization
- * Development Interface for incoming new features

Server-side Important Functions List:

     --------------------------
    | > Advanced Search        |
    | > BAEN Message Board     |
    | > App Indexing           |
    | > ......                 |
     --------------------------




Iteration Plan
=========================
Version 1.2: Optimization of Ad Details View

     ----------------
    | > Photo Browser|
    | >              |
    | >              |
     ----------------

Summary: In Progress